# RAVEN Project - UAV
## Rover and Air Environment Navigation

This package contains the launch files necessary to launch all ROS processes on the UAV companion computer for both indoor and outdoor flight tests.  
Requires packages:
- __pkg: mavros__ mavros node for communication with the PixRacer flight controller
- __pkg: testing\_resources__ _(optional)_ remap node to remap between vrpn motion capture data stream and mavros motion capture message.
    - launch arg: "mocap" default: true
- __pkg: rosbag__ _(optional)_ rosbag recording of all network topics
    - launch arg: "record default: false
- __pkg: testing\_resources__ _(optional)_ vrpn client node to communicate with VICON computer
    - launch arg: "vrpn" default: false

## Run Instructions

To run the launch file on the UAV companion computer:  
- First ssh into companion computer (password: odroid)
```
$ ssh odroid@<odroid ip address>
```
- Next, if the companion computer will not be the ROS master (recommended), set evironment variables  
```
odroid@odroid:$ export ROS_MASTER_URI=http://<master ip address>:11311
odroid@odroid:$ export ROS_IP=<odroid ip address>
```
- If it is desired to record data into a rosbag, you must mount the micro sd card on the companion computer and launch as root
```
odroid@odroid:$ sudo -s
odroid@odroid:# mount /dev/mmcblk1p1 /home/odroid/sdmnt
```
- Launching (must be root to record in ROSbag)
Indoor test
```
odroid@odroid:$ roslaunch uav indoor.launch
```
OR
```
odroid@odroid:# roslaunch uav indoor.launch record:=true
```
Outdoor test
```
odroid@odroid:$ roslaunch uav outdoor.launch
```
OR
```
odroid@odroid:# roslaunch uav outdoor.launch record:=true
```

## Running processes individually

It may be desirable to start each process that needs to run on the companion computure induvidually for debugging purposes, or if the launch file described above is causing problems.

__NOTE:__ each code block in this section assumes that  
1. you have sshed into the companion computer in this terminal window
2. you have set the ROS_MASTER_URI and ROS_IP environment variables in this terminal window

- If the vrpn client node is to be run on the companion computer
```
$ roslaunch testing_resources vicon_test.launch
```
- Launching the mavros node. _Note that if there are other USB devices connected, such as an Arduino, the fcu\_url might be USB1 instead of USB0._
```
$ roslaunch mavros px4.launch fcu_url:="/dev/ttyUSB0:921600"
```
- To start the remap from the vrpn motion capture data stream to the mavros topic
```
$ rosrun testing_resources remapper.py
```
- To record topic data to a rosbag. _Follow the instructions above to mount an sd card and run this process as root_
```
# rosbag record -o /home/odroid/sdmnt <topic names>
```
---
__Author: Ian Loefgren__  
__Email: ian.loefgren@colorado.edu__  
__Last updated: 03/28/2018__  