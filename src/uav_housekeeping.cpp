/*
*
*
*    Node: UAV Housekeeping 
*    Date: 03/20/2018
*    Author: Rishab Gangopadhyay
*
*    Vehicle: UAV
*
*
*    Purpose: This node throttles topics for compatibility with the UI.
*             It does so by subscribing to topics that are already being 
*             published through MAVROS, and then publishing out new formatted
*             msgs at a fixed rate of approximately 5 Hz. This low rate allows
*             for the UI to read from these topics successfully. 
*
*    Inputs:  MAVROS Altitude (/mavros/global_position/rel_alt
*             MAVROS Battery  (/mavros/battery)
*             MAVROS Temperature (/mavros/imu/temperature)
*             MAVROS Commanded Velocity (/mavros/setpoint_velocity/cmd_vel)
*
*    Outputs: Onboard storage information (/uav_storage_info)
*             WiFi Connection information (/uav_network_link_level_noise_ui)
*             Altitude (/uav_alt_ui)            
*             Battery (/uav_battvoltage_ui)
*             Temperature (/uav_temperature_ui)
*             Commanded Velocity (/uav_commanded_vel_ui) 
*
*/

/* ********************************************************************************
 *  Packages to Include 
 *
 **********************************************************************************/ 

#include <ros/ros.h>
#include <math.h>
#include "sensor_msgs/BatteryState.h"
#include "sensor_msgs/Temperature.h"
#include "geometry_msgs/Twist.h"
#include "geometry_msgs/TwistStamped.h"



#include <iostream>
#include <fstream>
#include <stdlib.h>
using namespace std;
#include "std_msgs/String.h"
#include "std_msgs/Bool.h"
#include "std_msgs/Float64.h"
#include "std_msgs/Float32.h"


/* ********************************************************************************
 *  Variable Definitions
 *
 **********************************************************************************/ 

//Diagnostics Variables Initialization
std_msgs::Float64 altitude;
std_msgs::Float64 uav_temp;

std_msgs::Float32 uav_voltage;

//Variables used to get network status
std::string network_status;
vector<string> network_data;
geometry_msgs::Vector3 network_msg;
const char networkcmdarr[] = "head -3 /proc/net/wireless | tail -1  | awk '{print $3 $4 $5}' 2>&1";


//Storage Information Variables
const char storage_info_cmdarr[] = "df -h -BG |grep mmcblk1p1 | awk '{print $2$3$4}' 2>&1";
std::string storage_info;
vector<string> storage_data;
geometry_msgs::Vector3 storage_data_vec;

//Msg for commanded velocity
geometry_msgs::TwistStamped commanded_velocity_out;

/* ********************************************************************************
 *  Callback Functions 
 *
 *  These callback functions are called by the ROS subscribers defined in main 
 *  whenever a new topic value comes in. The primary role of these callback 
 *  functions is to store a local copy of the most recent telemetry so that they
 *  can be output as required to maintain a 5Hz output
 **********************************************************************************/ 

//Callback function for UAV Temperature
void TempSubCallback(const sensor_msgs::Temperature::ConstPtr& msg)
{
    uav_temp.data = msg->temperature;
}

//Callback function for UAV Voltage
void BatterySubCallback(const sensor_msgs::BatteryState::ConstPtr& msg)
{
    uav_voltage.data = msg->voltage;
}

//Callback function for UAV Altitude
void AltSubCallback(const std_msgs::Float64::ConstPtr& msg)
{
    altitude.data = msg->data;

}

//Callback function for UAV commanded velocity
void VelSubCallback(const geometry_msgs::TwistStamped::ConstPtr& msg)
{
    commanded_velocity_out.header = msg->header;
    commanded_velocity_out.twist = msg->twist;
}


/* ********************************************************************************
 *  Helper Functions 
 *
 **********************************************************************************/ 


/* getCmdOutput
*
*  Purpose: to run a command line argument and store the output locally 
*  Modified from code by Vittorio Romeo 
*  https://codereview.stackexchange.com/questions/42148/
*  running-a-shell-command-and-getting-output */

std::string getCmdOutput(const std::string& mStr)
{
    std::string result, file;
    FILE* pipe{popen(mStr.c_str(), "r")};
    char buffer[256];

    while(fgets(buffer, sizeof(buffer), pipe) != NULL)
    {
        file = buffer;
        result += file.substr(0, file.size() - 1);
    }

    pclose(pipe);
    return result;
}

/* split
*
* Purpose: to take a string variable and split it into its various elements
*          by some predetermined delimiter
 http://ysonggit.github.io/coding/2014/12/16/split-a-string-using-c.html */
vector<string> split(const string &s, char delim) {
    stringstream ss(s);
    string item;
    vector<string> tokens;
    while (getline(ss, item, delim)) {
        tokens.push_back(item);
    }
    return tokens;
}

/* getStorageInfo
*
*  Purpose: this fumction spawns a shell command to run a command line
*           function in order to determine the storage space stize, 
*           used and avalailable on the mounted mmcblk1p1 drive. 
*           It then stores this information in a new message for 
*           publishing out.                                            */

void getStorageInfo()
{

        storage_info = getCmdOutput(storage_info_cmdarr);
        if(!storage_info.empty())
	{
	   storage_data = split(storage_info, 'G');
           storage_data_vec.x = atof(storage_data[0].c_str()); //Size
           storage_data_vec.y = atof(storage_data[1].c_str());//used
           storage_data_vec.z = atof(storage_data[2].c_str());//available
	}
	else
        {
	   storage_data_vec.x = 0.0;
	   storage_data_vec.y = 0.0;
	   storage_data_vec.z = 0.0;
        }
}

/* checkWiFiStrength
*
*  Purpose: this function runs a command line argument to read in information
*           from the Linux filesystems's /proc/wireless file, which provides
*           information regarding the connection status quality, link level, 
*           and noise level, and then stores that information for publishing */
void checkWiFiStrength()
{
        network_status = getCmdOutput(networkcmdarr);
        network_data = split(network_status, '.');
        network_msg.x = atof(network_data[0].c_str());
        network_msg.y = atof(network_data[1].c_str());
        network_msg.z = atof(network_data[2].c_str());
}

/* ********************************************************************************
 *  main 
 **********************************************************************************/ 

int main(int argc, char ** argv)
{


    // Initialize ROS node with name " "
    ros::init(argc, argv, "uav_housekeeping");
    // Create handle to acess node functionality
    ros::NodeHandle nh;
    
    ros::Rate loop_rate(5); // set rate for update loop [Hz]

    // Subscriber and Publisher for Altitude    
    ros::Subscriber alt_sub = nh.subscribe("/mavros/global_position/rel_alt", 100, AltSubCallback);
    ros::Publisher  alt_pub = nh.advertise<std_msgs::Float64>("/uav_alt_ui", 100);
    

    //Subscriber and Publisher for battery voltage
    ros::Subscriber battV_sub = nh.subscribe("/mavros/battery", 100, BatterySubCallback);
    ros::Publisher  battV_pub = nh.advertise<std_msgs::Float32>("/uav_battvoltage_ui", 100);

    //Create publisher for network
    ros::Publisher  network_pub = nh.advertise<geometry_msgs::Vector3>("/uav_network_link_level_noise_ui",100);
    //Create publsher for storage information
    ros::Publisher storage_info_pub = nh.advertise<geometry_msgs::Vector3>("/uav_storage_info",100);

    //Subscroiber and Publisher for Temperature
    ros::Subscriber temp_sub = nh.subscribe("/mavros/imu/temperature", 100, TempSubCallback);
    ros::Publisher  temp_pub = nh.advertise<std_msgs::Float64>("/uav_temperature_ui", 100);

    //Subscriber and Publisher for commanded velocity
    ros::Subscriber vel_sub = nh.subscribe("/mavros/setpoint_velocity/cmd_vel", 100, VelSubCallback);
    ros::Publisher  vel_pub = nh.advertise<geometry_msgs::TwistStamped>("/uav_commandedvel_ui", 100);

    // ros::ok()==0 occurs when node gets SIGTERM interrupt or similar
    while(ros::ok())
    { 

        
        // block once
        ros::spinOnce();
        // sleep for time defined by loop rate
        loop_rate.sleep();
        //Get storage information
        getStorageInfo();
	    //Get Wifi connection information
        checkWiFiStrength();
        //publish altitude topic
	    alt_pub.publish(altitude);
        //publish battery topic
        battV_pub.publish(uav_voltage);
        //publish network connection topic
        network_pub.publish(network_msg);
        //publish storage info topic
        storage_info_pub.publish(storage_data_vec);
        //publish temperature topic
        temp_pub.publish(uav_temp);
        //publish commanded velocity topic
        vel_pub.publish(commanded_velocity_out);   
        //Output ROS_INFO to record that we are running the housekeeping node. 
	    ROS_INFO("UAV_HOUSEKEEPING RUNNING");

    }
    return 0;
    
}
