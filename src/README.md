# RAVEN Project - UAV SRC Folder
## Rover and Air Environment Navigation

The locally created nodes for the UAV are listed in this folder
These nodes include:
- __uav_housekeeping.cpp__ node used for output topics to the UI
    - Reduces the publishing rate of certain topics to roughly 5Hz
    - Determines and publishes storage information of mounted drives
    - Determines and publishes wifi connection information
